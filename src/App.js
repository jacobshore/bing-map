import "./App.css";

import { Col, Container, Row } from "react-bootstrap";

import BingMap from "./components/BingMap";
import CoordinatesForm from "./components/CoordinatesForm";

function App() {
  return (
    <Container>
      <Row>
        <Col>
          <BingMap />
        </Col>
      </Row>
      <Col lg={3}>
        <CoordinatesForm />
      </Col>
    </Container>
  );
}

export default App;
