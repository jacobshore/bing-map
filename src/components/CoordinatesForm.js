import { CordsForm, PlaceForm } from "./Inputs";

import { Form } from "react-bootstrap";
import { useState } from "react";

export default function CoordinatesForm() {
  const [inputType, setInputType] = useState("cords");

  return (
    <Form>
      <Form.Group className="pb-3">
        <Form.Check
          type="radio"
          id="cords"
          name="inputType"
          label="Add to Cords"
          onFocus={(e) => setInputType(e.target.id)}
        />
        <Form.Check
          type="radio"
          id="place"
          name="inputType"
          label="Add a place"
          onFocus={(e) => setInputType(e.target.id)}
        />
      </Form.Group>
      <hr />
      {inputType === "cords" ? <CordsForm /> : <PlaceForm />}
    </Form>
  );
}
