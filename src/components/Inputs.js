import { Form } from "react-bootstrap";
import create from "zustand";

const useStore = create((set) => ({
  places: [],
  addPlace: (place) =>
    set((state) => ({ place: [...state.places, { place }] })),
}));

export const CordsForm = () => {
  return (
    <>
      <Form.Group>
        <Form.Label>Latitude</Form.Label>
        <Form.Control type="text" />
      </Form.Group>
      <Form.Group>
        <Form.Label>Longitude</Form.Label>
        <Form.Control type="text" />
      </Form.Group>
    </>
  );
};

export const PlaceForm = () => {
  //   const handleSubmit = (e) => {
  //     e.preventDefault();
  //   };
  //   const place = useStore((state) => state.places);
  return (
    <>
      <Form.Group>
        <Form.Label>Place</Form.Label>
        <Form.Control type="text" />
      </Form.Group>
    </>
  );
};
