import BingMapsReact from "bingmaps-react";

export default function BingMap() {
  return (
    <BingMapsReact
      bingMapsKey={process.env.REACT_APP_BING_KEY}
      height="500px"
      position="absolute"
      mapOptions={{
        navigationBarMode: "square",
      }}
      //   width="500px"
      viewOptions={{
        center: { latitude: 42.360081, longitude: -71.058884 },
        mapTypeId: "grayscale",
      }}
    />
  );
}
